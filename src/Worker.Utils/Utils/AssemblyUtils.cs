﻿using System.Reflection;

namespace Worker.Utils.Utils;

/// <summary>
/// Методы расширения и вспомгательные методы для работы со сборками
/// </summary>
public static class AssemblyUtils
{
    /// <summary>
    /// Получить все сборки загруженные в память, кроме динавических
    /// </summary>
    /// <returns>Список сборок</returns>
    public static IEnumerable<Assembly> GetLoadedAssemblies() =>
        AppDomain.CurrentDomain.GetAssemblies().Where(x => !x.IsDynamic);

    /// <summary>
    /// Получить все публичные типы из загруженных сборок
    /// </summary>
    /// <returns>Список всех типов</returns>
    public static IEnumerable<Type> GetAllExportTypes() =>
        GetLoadedAssemblies().SelectMany(x => x.ExportedTypes);
    
    /// <summary>
    /// Получить все публичные классы имплементирующие класс/интерфейс типа <paramref name="baseType"/>
    /// </summary>
    /// <param name="baseType">Базовый тип</param>
    /// <param name="nonAbstract">Искать абстракные?</param>
    /// <returns>Список типов</returns>
    public static IEnumerable<TypeInfo> GetAllExportTypesInfoBasedOn(Type baseType, bool nonAbstract = true) =>
        GetAllExportTypes()
            .Where(baseType.IsAssignableFrom)
            .Where(x => !nonAbstract || !x.IsAbstract)
            .Select(x => x.GetTypeInfo());


    /// <summary>
    /// Получить все публичные классы имплементирующие класс/интерфейс типа <paramref name="T"/>
    /// </summary>
    /// <param name="nonAbstract">Искать абстракные?</param>
    /// <returns>Список типов</returns>
    public static IEnumerable<TypeInfo> GetAllExportTypesInfoBasedOn<T>(bool nonAbstract = true) =>
        GetAllExportTypesInfoBasedOn(typeof(T), nonAbstract);
    
    /// <summary>
    /// Получить все публичные классы имплементирующие класс/интерфейс типа <paramref name="T"/>
    /// </summary>
    /// <param name="nonAbstract">Искать абстракные?</param>
    /// <returns>Список типов</returns>
    public static IEnumerable<Type> GetAllExportTypesBasedOn<T>(bool nonAbstract = true) =>
        GetAllExportTypesInfoBasedOn<T>(nonAbstract);

    /// <summary>
    /// Получить все публичные классы имплементирующие класс/интерфейс типа <paramref name="baseType"/>
    /// </summary>
    /// <param name="baseType">Базовый тип</param>
    /// <param name="nonAbstract">Искать абстракные?</param>
    /// <returns>Список типов</returns>
    public static IEnumerable<Type> GetAllExportTypesBasedOn(Type baseType, bool nonAbstract = true) =>
        GetAllExportTypesInfoBasedOn(baseType, nonAbstract);

    /// <summary>
    /// Получить все публичные классы имплементирующие класс/интерфейс типа <paramref name="baseType"/>
    /// </summary>
    /// <param name="baseType">Базовый тип</param>
    /// <param name="nonAbstract">Искать абстракные?</param>
    /// <param name="includeNonPublic">Искать приватные?</param>
    /// <returns>Список типов</returns>
    public static IEnumerable<TypeInfo> GetAllTypesInfoBasedOn(Type baseType, bool nonAbstract = true,
        bool includeNonPublic = false) =>
        GetLoadedAssemblies().SelectMany(x => includeNonPublic ? x.DefinedTypes : x.ExportedTypes)
            .Where(baseType.IsAssignableFrom)
            .Where(x => !nonAbstract || !x.IsAbstract)
            .Select(x => x.GetTypeInfo());
}