using Autofac;
using Autofac.Extensions.DependencyInjection;
using Worker.Core.Configuration;
using static Worker.Core.DependencyInjection.Environment;

var host = Host.CreateDefaultBuilder(args)
    .ConfigureAppConfiguration((context, config) => 
    {
        config.SetBasePath(Directory.GetCurrentDirectory());
        config.AddJsonFile("appsettings.json", false);
        config.AddJsonFile($"appsettings.{context.HostingEnvironment.EnvironmentName}.json", true, false);
        config.AddEnvironmentVariables();
    })
    .UseServiceProviderFactory(new AutofacServiceProviderFactory())
    .ConfigureServices(ConfigureServices)
    .ConfigureContainer<ContainerBuilder>(RegisterModules)
    .Build();

Init(host);

await host.RunAsync();

void ConfigureServices(HostBuilderContext context, IServiceCollection services)
{
    services.AddSingleton(GetConfiguration(context.Configuration));
    services.AddHostedService<HostedService>();
}

WorkerConfiguration GetConfiguration(IConfiguration configuration) 
{
    var workerConfiguration = new WorkerConfiguration();
    configuration.GetSection(nameof(WorkerConfiguration)).Bind(workerConfiguration);

    return workerConfiguration;
}
