using Autofac;
using Worker.Core.Configuration;
using Worker.Core.DependencyInjection;
using Environment = Worker.Core.DependencyInjection.Environment;

internal sealed class HostedService : IHostedService
{
    private readonly IHostApplicationLifetime _appLifetime;

    private readonly ILogger<HostedService> _logger;
    
    private readonly WorkerConfiguration _configuration;

    public HostedService(IHostApplicationLifetime appLifetime, WorkerConfiguration configuration, ILogger<HostedService> logger)
    {
        _appLifetime = appLifetime;
        _configuration = configuration;
        _logger = logger;
    }
    
    public Task StartAsync(CancellationToken cancellationToken)
    {
        _appLifetime.ApplicationStarted.Register(OnStarted);
        _appLifetime.ApplicationStopping.Register(OnStopping);
        _appLifetime.ApplicationStopped.Register(OnStopped);
        
        return Task.CompletedTask;
    }
    
    public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;

    private void OnStarted()
    {
        var config = Environment.GetLocalRawRabbitConfiguration(_configuration, 60);

        using var point = Environment.CreatePoint(config);
            
        point.Open();
        
        point.Server.SubscribeAsync<string>(async (msg, context) =>
        {
            Console.WriteLine("Pong");
        });
    }
    
    private void OnStopping()
    {
        
    }

    private void OnStopped()
    {
        
    }
}