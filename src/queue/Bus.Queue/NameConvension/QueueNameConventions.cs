﻿namespace Bus.Queue.NameConvension
{
    /// <summary>
    /// Соглашение о именах членов и классов для очереди задач
    /// </summary>
    public class QueueNameConventions
    {
        /// <summary>
        /// Получить имя хеш-таблицы для блокировки
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetLockTableName(string key) => string.Intern($"{key}:lock-queue");
        
        /// <summary>
        /// Получить имя хеш-таблицы для списка задач в очереди
        /// (все, включая активные задачи)
        /// </summary>
        /// <param name="virtualHost">Виртуальный хост</param>
        /// <returns>Таблица задач в очереди</returns>
        public static string GetTableInQueue(string virtualHost) => string.Intern($"{virtualHost}:tasks-in-queue");

        /// <summary>
        /// Получить имя хеш-таблицы для списка задач в процессе выполнения
        /// </summary>
        /// <param name="virtualHost">Виртуальный хост</param>
        /// <returns>Таблица задач, которые еще выполняются</returns>
        public static string GetTableInProcess(string virtualHost) => string.Intern($"{virtualHost}:tasks-in-process");
    }
}