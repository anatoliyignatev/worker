﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Bus.Transport.Context;
using RawRabbit.Configuration.Publish;
using RawRabbit.Configuration.Request;

namespace Bus.Transport.Client.Interface
{
    /// <summary>
    /// Клиент транспорта сообщений
    /// </summary>
    public interface ITransportClient <out TMessageContext> : IDisposable where TMessageContext : BaseMessageContext
    {
        /// <summary>
        /// Выполнить запрос
        /// </summary>
        /// <typeparam name="TRequest">Тип запроса</typeparam>
        /// <typeparam name="TResponse">Тип ответа</typeparam>
        /// <param name="message">Объект запроса</param>
        /// <param name="token">Токен отмены запроса</param>
        /// <returns></returns>
        Task<TResponse> RequestAsync<TRequest, TResponse>(TRequest message, CancellationToken token = default(CancellationToken));

        /// <summary>
        /// Выполнить запрос
        /// </summary>
        /// <typeparam name="TRequest">Тип запроса</typeparam>
        /// <typeparam name="TResponse">Тип ответа</typeparam>
        /// <param name="message">Объект запроса</param>
        /// <param name="configuration">Конфигурация запроса</param>
        /// <param name="token">Токен отмены запроса</param>
        /// <returns></returns>
        Task<TResponse> RequestAsync<TRequest, TResponse>(
            TRequest message,
            Action<IRequestConfigurationBuilder> configuration,
            CancellationToken token = default(CancellationToken));
		
        /// <summary>
        /// Опубликовать событие типа <see cref="TMessage"/>
        /// </summary>
        /// <typeparam name="TMessage">Тип события</typeparam>
        /// <param name="message">Объект события</param>
        /// <param name="token">Токен для отмены обработки события</param>
        /// <returns></returns>
        Task PublishAsync<TMessage>(TMessage message, CancellationToken token = default(CancellationToken));

        /// <summary>
        /// Опубликовать событие типа <see cref="TMessage"/>
        /// </summary>
        /// <typeparam name="TMessage">Тип события</typeparam>
        /// <param name="message">Объект события</param>
        /// <param name="configuration">Конфигурация для публикации</param>
        /// <param name="token">Токен для отмены обработки события</param>
        /// <returns></returns>
        Task PublishAsync<TMessage>(
            TMessage message, 
            Action<IPublishConfigurationBuilder> configuration, 
            CancellationToken token = default(CancellationToken));
    }
}