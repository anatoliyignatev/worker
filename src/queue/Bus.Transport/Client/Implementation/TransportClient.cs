﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Bus.Transport.Cancelation;
using Bus.Transport.Client.Interface;
using Bus.Transport.Context;
using RawRabbit.Configuration.Publish;
using RawRabbit.Configuration.Request;
using RawRabbit.vNext.Disposable;


namespace Bus.Transport.Client.Implementation
{
    public class TransportClient<TMessageContext> : ITransportClient<TMessageContext>
        where TMessageContext : BaseMessageContext, new()
    {
        /// <summary>
        /// Клиент доступа к шине
        /// </summary>
        private IBusClient<TMessageContext> BusClient { get; }
        
        /// <summary>
        /// создать новый экземпляр
        /// </summary>
        public TransportClient(IBusClient<TMessageContext> busClient)
        {
            BusClient = busClient;
        }
        
        /// <summary>
        /// Опубликовать событие типа <see cref="TMessage"/>
        /// </summary>
        /// <typeparam name="TMessage">Тип события</typeparam>
        /// <param name="message">Объект события</param>
        /// <param name="token">Токен для отмены обработки события</param>
        /// <returns></returns>
        public Task PublishAsync<TMessage>(TMessage message, CancellationToken token = default)
        {
            return PublishAsync(message, null, token);
        }
        
        /// <summary>
        /// Опубликовать событие типа <see cref="!:TMessage" />
        /// </summary>
        /// <typeparam name="TMessage">Тип события</typeparam>
        /// <param name="message">Объект события</param>
        /// <param name="configuration">Конфигурация для публикации</param>
        /// <param name="token">Токен для отмены обработки события</param>
        /// <returns></returns>
        public Task PublishAsync<TMessage>(
            TMessage message, 
            Action<IPublishConfigurationBuilder> configuration,
            CancellationToken token = default)
        {
            var globalMessageId = Register(token);
            return BusClient.PublishAsync(message, globalMessageId, configuration);
        }
        
        /// <summary>
        /// Выполнить запрос
        /// </summary>
        /// <typeparam name="TRequest">Тип запроса</typeparam>
        /// <typeparam name="TResponse">Тип ответа</typeparam>
        /// <param name="message">Объект запроса</param>
        /// <param name="token">Токен отмены запроса</param>
        /// <returns></returns>
        public Task<TResponse> RequestAsync<TRequest, TResponse>(TRequest message,
            CancellationToken token = default)
        {
            return RequestAsync<TRequest, TResponse>(message, null, token);
        }

        /// <summary>
        /// Выполнить запрос
        /// </summary>
        /// <typeparam name="TRequest">Тип запроса</typeparam>
        /// <typeparam name="TResponse">Тип ответа</typeparam>
        /// <param name="message">Объект запроса</param>
        /// <param name="configuration">Конфигурация запроса</param> 
        /// <param name="token">Токен отмены запроса</param>
        /// <returns></returns>
        public Task<TResponse> RequestAsync<TRequest, TResponse>(TRequest message, Action<IRequestConfigurationBuilder> configuration,
            CancellationToken token = default)
        {
            var globalMessageId = Register(token);
            return BusClient.RequestAsync<TRequest, TResponse>(message, globalMessageId, configuration);
        }
        
        /// <summary>
        /// Регистрируем подписку на событие отмены
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        private Guid Register(CancellationToken token)
        {
            var globalMessageId = Guid.NewGuid();
            if (token != default)
            {
                //при запросе отмены публикуем запрос типа CancellationRequest
                token.Register(
                    ob =>
                    {
                        BusClient.PublishAsync(new CancellationRequest
                            {
                                Guid = (Guid) ob, 
                                LibType = LibTypes.Bus
                            },
                            configuration: builder => builder.WithExchange(x => x.WithAutoDelete()));
                    },
                    globalMessageId);
            }

            return globalMessageId;
        }

        public void Dispose()
        {
        }
    }
}