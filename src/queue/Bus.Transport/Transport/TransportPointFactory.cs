﻿using System;
using Bus.Transport.Context;
using Microsoft.Extensions.DependencyInjection;
using RawRabbit.Configuration;

namespace Bus.Transport.Transport
{
    /// <summary>
    /// Фабрика транспортных точек
    /// </summary>
    public static class TransportPointFactory
    {
        /// <summary>
        /// Создать точку доступа к шине сообщений со стандартным контекстом
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="userSettings"></param>
        /// <returns></returns>
        public static TransportPoint<BaseMessageContext> CreatePoint(RawRabbitConfiguration configuration, Action<IServiceCollection> userSettings)
        {
            return new TransportPoint<BaseMessageContext>(configuration, userSettings);
        }
    }
}