﻿using System;
using Bus.Transport.Client.Implementation;
using Bus.Transport.Client.Interface;
using Bus.Transport.Context;
using Bus.Transport.Server.Implementation;
using Bus.Transport.Server.Interface;
using Bus.Transport.Transport.WithState;
using Microsoft.Extensions.DependencyInjection;
using RawRabbit.Configuration;
using RawRabbit.vNext;
using RawRabbit.vNext.Disposable;

namespace Bus.Transport.Transport
{
    public class TransportPoint<TMessageContext> : Point, IDisposable where TMessageContext : BaseMessageContext, new()
    {
        /// <summary>
        /// Тип состояния точки доступа
        /// </summary>
        public TransportPointStates State { get; private set; }
        
        /// <summary>
        /// Признак наличия подписки на события отмены
        /// </summary>
        public bool EnableCancellationSubscribe
        {
            get => PointState.EnableCancellationSubscribe;
            set => PointState.EnableCancellationSubscribe = value;
        }
        
        /// <summary>
        /// Клиент транспорта сообщений
        /// </summary>
        public ITransportClient<TMessageContext> Client => PointState.Client;

        /// <summary>
        /// Сервер транспорта сообщений
        /// </summary>
        public ITransportServer<TMessageContext> Server => PointState.Server;
        
        /// <summary>
        /// Объект состояния точки доступа
        /// </summary>
        private TransportPointState<TMessageContext> PointState { get; set; }
        
        /// <summary>
        /// Клиент для работы с шиной
        /// </summary>
        private IBusClient<TMessageContext> BusClient { get; set; }
        
        private readonly object _syncObject = new();

        private readonly IServiceProvider _serviceProvider;
        
        
        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        internal TransportPoint(RawRabbitConfiguration configuration, Action<IServiceCollection> serviceSettings)
            : base(configuration)
        {
            ServiceSettings += collection => collection.AddSingleton<ITransportClient<TMessageContext>, TransportClient<TMessageContext>>();
            ServiceSettings += serviceSettings;
            State = TransportPointStates.Closed;
            _serviceProvider = ServiceProviderFactory.CreateServiceProvider<TMessageContext>(RawRabbitConfiguration, ServiceSettings);
            PointState = new ClosedTransportPoint<TMessageContext>();
        }
        
        /// <summary>
        /// Открыть точку доступа
        /// </summary>
        public void Open()
        {
            lock (_syncObject)
            {
                if (State == TransportPointStates.Opened)
                {
                    return;
                }
                
                BusClient = _serviceProvider.GetRequiredService<IBusClient<TMessageContext>>();
                PointState = new OpenTransportPoint<TMessageContext>(PointState.EnableCancellationSubscribe)
                {
                    Client = new TransportClient<TMessageContext>(BusClient),
                    Server = new TransportServer<TMessageContext>(BusClient, PointState.EnableCancellationSubscribe)
                };
                
                State = TransportPointStates.Opened;
            }
        }
        
        /// <summary>
        /// Закрыть точку доступа
        /// </summary>
        public void Close()
        {
            lock (_syncObject)
            {
                if (State == TransportPointStates.Closed)
                {
                    return;
                }

                Client?.Dispose();
                Server?.Dispose();
                State = TransportPointStates.Closed;
                PointState = new ClosedTransportPoint<TMessageContext>();
            }
        }
        
        public void Dispose()
        {
            Close();
        }
    }
}