﻿namespace Bus.Transport.Transport.WithState
{
    /// <summary>
    /// Состояния точки доступа к шине
    /// </summary>
    public enum TransportPointStates
    {
        /// <summary>
        /// Точка доступа закрыта
        /// </summary>
        Closed,

        /// <summary>
        /// Точка доступа открыта
        /// </summary>
        Opened
    }
}