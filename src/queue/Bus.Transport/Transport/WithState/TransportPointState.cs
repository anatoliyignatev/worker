﻿using Bus.Transport.Client.Interface;
using Bus.Transport.Context;
using Bus.Transport.Server.Interface;

namespace Bus.Transport.Transport.WithState
{
    /// <summary>
    /// Состояние точки доступа
    /// </summary>
    public abstract class TransportPointState<TMessageContext> where TMessageContext : BaseMessageContext
    {
        /// <summary>
        /// Клиент транспорта сообщений
        /// </summary>
        public abstract ITransportClient<TMessageContext> Client { get; set; }

        /// <summary>
        /// Сервер транспорта сообщений
        /// </summary>
        public abstract ITransportServer<TMessageContext> Server { get; set; }

        /// <summary>
        /// Признак наличия подписки на события отмены
        /// </summary>
        public abstract bool EnableCancellationSubscribe { get; set; }
    }
}