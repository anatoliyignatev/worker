﻿using System;
using Bus.Transport.Client.Interface;
using Bus.Transport.Context;
using Bus.Transport.Server.Interface;

namespace Bus.Transport.Transport.WithState
{
    /// <summary>
    /// Открытая точка доступа к шине
    /// </summary>
    public class OpenTransportPoint<TMessageContext> : TransportPointState<TMessageContext>
        where TMessageContext : BaseMessageContext
    {
        private readonly bool _enableCancellationSubscribe;

        public OpenTransportPoint(bool enableCancellationSubscribe)
        {
            _enableCancellationSubscribe = enableCancellationSubscribe;
        }

        /// <summary>
        /// Клиент транспорта сообщений
        /// </summary>
        public override ITransportClient<TMessageContext> Client { get; set; }

        /// <summary>
        /// Сервер транспорта сообщений
        /// </summary>
        public override ITransportServer<TMessageContext> Server { get; set; }

        /// <summary>
        /// Признак наличия подписки на события отмены
        /// </summary>
        public override bool EnableCancellationSubscribe
        {
            get => _enableCancellationSubscribe;
            set => throw InternalException;
        }

        private Exception InternalException =>
            new Exception($"Точка доступа к шине находится в состоянии {nameof(TransportPointStates.Opened)}.");
    }
}