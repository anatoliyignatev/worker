﻿using System;
using Bus.Transport.Client.Interface;
using Bus.Transport.Context;
using Bus.Transport.Server.Interface;

namespace Bus.Transport.Transport.WithState
{
    public class ClosedTransportPoint<TMessageContext> : TransportPointState<TMessageContext> where TMessageContext : BaseMessageContext
    {
        /// <summary>
        /// Клиент транспорта сообщений
        /// </summary>
        public override ITransportClient<TMessageContext> Client
        {
            get => throw InternalException;
            set => throw InternalException;
        }

        /// <summary>
        /// Сервер транспорта сообщений
        /// </summary>
        public override ITransportServer<TMessageContext> Server
        {
            get => throw InternalException;
            set => throw InternalException;
        }

        /// <summary>
        /// Признак наличия подписки на события отмены
        /// </summary>
        public override bool EnableCancellationSubscribe { get; set; } = true;

        private InvalidOperationException InternalException =>
            new($"Точка доступа к шине находится в состоянии {nameof(TransportPointStates.Closed)}.");
    }
}