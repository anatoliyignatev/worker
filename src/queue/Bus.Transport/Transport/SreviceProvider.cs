﻿using System;
using Bus.Transport.Context;
using Microsoft.Extensions.DependencyInjection;
using RawRabbit.Configuration;
using RawRabbit.vNext;
using RawRabbit.vNext.Disposable;

namespace Bus.Transport.Transport
{
    	/// <summary>
	/// Фабрика <see cref="IServiceProvider"/>
	/// </summary>
	public static class ServiceProviderFactory
	{
		/// <summary>
		/// Создать клиента шины <see cref="IBusClient"/> на основе конфигурации <see cref="configuration"/>
		/// </summary>
		/// <param name="configuration"></param>
		/// <param name="userSettings"></param>
		/// <returns></returns>
		public static IServiceProvider CreateServiceProvider<TMessageContext>(RawRabbitConfiguration configuration, Action<IServiceCollection> userSettings) where TMessageContext : BaseMessageContext, new()
		{
			var action = (s => s.AddSingleton(_ => configuration)) + userSettings;
			
		    var serviceCollection = new ServiceCollection()
			    .AddRawRabbit<TMessageContext>(null, action)
			    .AddSingleton<IBusClient<TMessageContext>>(provider =>
				    new BusClient<TMessageContext>(provider.GetService<RawRabbit.IBusClient<TMessageContext>>()));
		    
		    return serviceCollection.BuildServiceProvider();
		}
    }
}