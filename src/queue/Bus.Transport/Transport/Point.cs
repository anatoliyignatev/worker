﻿using System;
using Microsoft.Extensions.DependencyInjection;
using RawRabbit.Configuration;

namespace Bus.Transport.Transport
{
    public abstract class Point
    {
        /// <summary>
        /// Конфигурация точки доступа к шине
        /// </summary>
        public RawRabbitConfiguration RawRabbitConfiguration { get; }

        /// <summary>
        /// Коллекция контрактов 
        /// </summary>
        protected Action<IServiceCollection> ServiceSettings;

        /// <summary>
        /// Уникальный идентификатор точки
        /// </summary>
        public Guid Id { get; }

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        protected Point(RawRabbitConfiguration rawRabbitConfiguration)
        {
            Id = Guid.NewGuid();

            RawRabbitConfiguration = rawRabbitConfiguration;
        }
    }
}