﻿using System;

namespace Bus.Transport.NameConvension
{
    public static class TransportNameConvension
    {
        /// <summary>
        /// Получить уникальное имя очереди для подписки на сообщение об отмене
        /// </summary>
        public static string GetCancellationQueueName => $"CancellationBusQueue_{Guid.NewGuid().ToString()}";
    }
}