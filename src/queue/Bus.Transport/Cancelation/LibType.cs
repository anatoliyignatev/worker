﻿namespace Bus.Transport.Cancelation
{
    /// <summary>
    /// Типы библиотек
    /// </summary>
    public enum LibTypes
    {
        /// <summary>
        /// Неопределенный тип
        /// </summary>
        None =0, 
        
        /// <summary>
        /// Шина сообщений
        /// </summary>
        Bus = 1,
        
        /// <summary>
        /// Очередь задач
        /// </summary>
        Queue = 2
    }
}