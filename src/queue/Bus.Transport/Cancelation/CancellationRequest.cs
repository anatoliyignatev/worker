﻿
using System;
using RawRabbit.Attributes;
using RawRabbit.Configuration.Exchange;

namespace Bus.Transport.Cancelation
{
    /// <summary>
    /// Запрос на отмену
    /// </summary>
    [Exchange(Type = ExchangeType.Fanout, Name = ExchangeName)]
    public class CancellationRequest
    {
        /// <summary>
        /// Идентификатор токена для запроса отмены
        /// </summary>
        public Guid Guid { get; set; }
		
        /// <summary>
        /// Тип библиотеки с отменой
        /// </summary>
        public LibTypes LibType { get; set; }
		
        /// <summary>
        /// Виртуальный хост инициатора отмены
        /// </summary>
        public string VirtualHost { get; set; }
		
        /// <summary>
        /// Имя приложения-инициатора отмены
        /// </summary>
        public string ProcessName { get; set; }
		
        /// <summary>
        /// Комментарий при отмене
        /// </summary>
        public string Comment { get; set; }

        /// <summary>Returns a string that represents the current object.</summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return
                $"{nameof(Guid)}: {Guid}, {nameof(LibType)}: {LibType.ToString()}," +
                $" {nameof(VirtualHost)}: {VirtualHost ?? "NULL"}, {nameof(ProcessName)}: {ProcessName ?? "NULL"}," +
                $" {nameof(Comment)}: {Comment ?? "NULL"}";
        }

        /// <summary>
        /// Имя обменника
        /// </summary>
        internal const string ExchangeName = "CancellationFanout";
    }
}