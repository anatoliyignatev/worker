﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Bus.Transport.Cancelation;
using Bus.Transport.Context;
using Bus.Transport.NameConvension;
using Bus.Transport.Server.Interface;
using RawRabbit.Common;
using RawRabbit.Configuration.Respond;
using RawRabbit.Configuration.Subscribe;
using RawRabbit.vNext.Disposable;

namespace Bus.Transport.Server.Implementation
{
    /// <summary>
    /// Реализация сервера транспорта сообщений по-умолчанию
    /// </summary>
    public class TransportServer<TMessageContext> : ITransportServer<TMessageContext> where TMessageContext : BaseMessageContext, new()
    {
        /// <summary>
        /// Клиент доступа к шине
        /// </summary>
        private IBusClient<TMessageContext> BusClient { get; }
        
        private readonly ConcurrentDictionary<Guid, CancellationTokenSource> _cancellationTokenSources = new();
        
        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        public TransportServer(IBusClient<TMessageContext> busClient, bool enableCancellationSubscribe)
        {
            BusClient = busClient;
            ContextManager.RespondCompleted += DeleteCancellationByGuid;
            if (enableCancellationSubscribe)
            {
                SubscribeOnCancellations();
            }
        }
        
        /// <summary>
        /// Ответить на запрос
        /// </summary>
        /// <typeparam name="TRequest">Тип запроса</typeparam>
        /// <typeparam name="TResponse">Тип ответа</typeparam>
        /// <param name="handler">Обработка запроса</param>
        /// <returns></returns>
        public ISubscription RespondAsync<TRequest, TResponse>(Func<TRequest, TMessageContext, Task<TResponse>> handler)
        {
            return RespondAsync(handler, null);
        }

        /// <summary>
        /// Подписаться на событие типа <see cref="TMessage"/>
        /// </summary>
        /// <typeparam name="TMessage">Тип события</typeparam>
        /// <param name="subscribeMethod"></param>
        /// <returns></returns>
        public ISubscription SubscribeAsync<TMessage>(Func<TMessage, TMessageContext, Task> subscribeMethod)
        {
            return SubscribeAsync(subscribeMethod, null);
        }
        
        /// <summary>
        /// Подписаться на событие типа <see cref="TMessage"/>
        /// </summary>
        /// <typeparam name="TMessage">Тип события</typeparam>
        /// <param name="subscribeMethod"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public ISubscription SubscribeAsync<TMessage>(Func<TMessage, TMessageContext, Task> subscribeMethod,
            Action<ISubscriptionConfigurationBuilder> configuration)
        {
            return BusClient.SubscribeAsync<TMessage>((message, context) =>
            {
                RegisterCancellation(context);
                return subscribeMethod(message, context);
            }, configuration);
        }
        
        /// <summary>
        /// Ответить на запрос
        /// </summary>
        /// <typeparam name="TRequest">Тип запроса</typeparam>
        /// <typeparam name="TResponse">Тип ответа</typeparam>
        /// <param name="handler">Обработка запроса</param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public ISubscription RespondAsync<TRequest, TResponse>(
            Func<TRequest, TMessageContext, Task<TResponse>> handler,
            Action<IResponderConfigurationBuilder> configuration)
        {
            return BusClient.RespondAsync<TRequest, TResponse>((request, context) =>
            {
                try
                {
                    RegisterCancellation(context);
                    var task = handler(request, context);
                    return task;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }, configuration);
        }
        
        /// <summary>
        /// Зарегистрировать токен отмены для запроса
        /// </summary>
        /// <param name="context"></param>
        private void RegisterCancellation(TMessageContext context)
        {
            if (context.GlobalRequestId != Guid.Empty)
            {
                var cts = new CancellationTokenSource();
                _cancellationTokenSources.TryAdd(context.GlobalRequestId, cts);
                context.CancellationToken = cts.Token;
            }
        }
        
        /// <summary>
        /// Подписаться на сообщение об отмене задания
        /// </summary>
        private void SubscribeOnCancellations()
        {
            BusClient.SubscribeAsync<CancellationRequest>((request, _) =>
            {
                var guid = request.Guid;
                if (_cancellationTokenSources.TryGetValue(guid, out var cts))
                {
                    Console.WriteLine($"Запрошена отмена выполнения запроса с Id=\"{guid}\": [{request}] ");
                    cts.Cancel();
                }

                return Task.FromResult(true);
            }, builder => builder.WithExchange(x => x.WithAutoDelete().WithDurability(false)).WithQueue(configurationBuilder =>
                configurationBuilder.WithName(TransportNameConvension.GetCancellationQueueName).WithAutoDelete().WithDurability(false)));
        }

        /// <summary>
        /// Удалить токен отмены после завершения запроса
        /// </summary>
        /// <param name="guid"></param>
        private void DeleteCancellationByGuid(Guid guid)
        {
            _cancellationTokenSources.TryRemove(guid, out _);
        }

        /// <inheritdoc />
        public void Dispose()
        {
            ContextManager.RespondCompleted -= DeleteCancellationByGuid;
        }
    }
}