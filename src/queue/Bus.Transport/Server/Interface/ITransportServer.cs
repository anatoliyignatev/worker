﻿using System;
using System.Threading.Tasks;
using Bus.Transport.Context;
using RawRabbit.Common;
using RawRabbit.Configuration.Respond;
using RawRabbit.Configuration.Subscribe;

namespace Bus.Transport.Server.Interface
{
    /// <summary>
    /// Сервер транспорта сообщений
    /// </summary>
    public interface ITransportServer<out TMessageContext> : IDisposable where TMessageContext : BaseMessageContext
    {
        /// <summary>
        /// Ответить на запрос
        /// </summary>
        /// <typeparam name="TRequest">Тип запроса</typeparam>
        /// <typeparam name="TResponse">Тип ответа</typeparam>
        /// <param name="handler">Обработка запроса</param>
        /// <returns></returns>
        ISubscription RespondAsync<TRequest, TResponse>(Func<TRequest, TMessageContext, Task<TResponse>> handler);

        /// <summary>
        /// Подписаться на событие типа <see cref="TMessage"/>
        /// </summary>
        /// <typeparam name="TMessage">Тип события</typeparam>
        /// <param name="subscribeMethod">Метод обработки</param>
        /// <returns></returns>
        ISubscription SubscribeAsync<TMessage>(Func<TMessage, TMessageContext, Task> subscribeMethod);
		
        /// <summary>
        /// Ответить на запрос
        /// </summary>
        /// <typeparam name="TRequest">Тип запроса</typeparam>
        /// <typeparam name="TResponse">Тип ответа</typeparam>
        /// <param name="handler">Обработка запроса</param>
        /// <param name="configuration">Конфигурация для подписки</param>
        /// <returns></returns>
        ISubscription RespondAsync<TRequest, TResponse>(Func<TRequest, TMessageContext, Task<TResponse>> handler,
            Action<IResponderConfigurationBuilder> configuration);

        /// <summary>
        /// Подписаться на событие типа <see cref="TMessage"/>
        /// </summary>
        /// <typeparam name="TMessage">Тип события</typeparam>
        /// <param name="subscribeMethod">Метод обработки</param>
        /// <param name="configuration">Конфигурация для подписки</param>
        /// <returns></returns>
        ISubscription SubscribeAsync<TMessage>(Func<TMessage, TMessageContext, Task> subscribeMethod,
            Action<ISubscriptionConfigurationBuilder> configuration);
    }
}