﻿using System;

namespace Bus.Transport.Context
{
    internal static class ContextManager
    {
        internal static event Action<Guid> RespondCompleted;

        /// <summary>
        /// Удалить токен отмены и контекст сообщения
        /// </summary>
        /// <param name="requestGuid"></param>
        internal static void DeleteCancellationTokenSourceAndContext(Guid requestGuid)
        {
            RespondCompleted?.Invoke(requestGuid);
        }
    }
}