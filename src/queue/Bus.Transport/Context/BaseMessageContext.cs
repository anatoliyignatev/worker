﻿using System.Threading;
using RawRabbit.Context;

namespace Bus.Transport.Context
{
    /// <summary>
    /// Базовый контекст шины сообщений
    /// </summary>
    public class BaseMessageContext : AdvancedMessageContext 
    {
        /// <summary>
        /// Токен отмены выполнения задачи
        /// </summary>
        public CancellationToken CancellationToken { get; set; } = CancellationToken.None;
    }
}