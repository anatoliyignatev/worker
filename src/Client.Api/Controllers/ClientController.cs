using Microsoft.AspNetCore.Mvc;
using Worker.Core.Configuration;
using static Worker.Core.DependencyInjection.Environment;

namespace Client.Api.Controllers;

[ApiController]
[Route("api/[controller]")]
public class ClientController : ControllerBase
{
    private readonly WorkerConfiguration _configuration;
    
    private readonly ILogger<ClientController> _logger;

    public ClientController(WorkerConfiguration configuration, ILogger<ClientController> logger)
    {
        _configuration = configuration;
        _logger = logger;
    }

    [HttpGet("/pong")]
    public void Ping()
    {
        try
        {
            var config = GetLocalRawRabbitConfiguration(_configuration, 60);

            using var point = CreatePoint(config);

            point.Open();
            
            point.Client.PublishAsync("Ping");
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }
}