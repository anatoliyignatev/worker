using Microsoft.OpenApi.Models;
using Worker.Core.Configuration;

var builder = WebApplication.CreateBuilder(args);

builder.WebHost.ConfigureAppConfiguration((context, config) =>
{
    config.SetBasePath(Directory.GetCurrentDirectory());
    config.AddJsonFile("appsettings.json", false);
    config.AddJsonFile($"appsettings.{context.HostingEnvironment.EnvironmentName}.json", true, false);
    config.AddEnvironmentVariables();
});

var workerConfiguration = new WorkerConfiguration();
builder.Configuration.GetSection(nameof(WorkerConfiguration)).Bind(workerConfiguration);

builder.Services.AddSingleton(workerConfiguration);
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo { Title = "Test API", Version = "v1" });
});

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger(option => { option.RouteTemplate = "swagger/{documentName}/swagger.json"; });
    app.UseSwaggerUI(option =>
    {
        option.SwaggerEndpoint("v1/swagger.json", "TestApi");
    });
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();