﻿namespace Worker.Core.Configuration;

/// <summary>
/// Конфигурация RabbitMq
/// </summary>
public class RabbitMqOptions
{
    /// <summary>
    /// Строка подключения
    /// </summary>
    public string Address { get; set; }

    /// <summary>
    /// Виртуальный хост
    /// </summary>
    public string VirtualHost { get; set; }

    /// <summary>
    /// Пользователь
    /// </summary>
    public string UserName { get; set; }

    /// <summary>
    /// Пароль
    /// </summary>
    public string Password { get; set; }
}