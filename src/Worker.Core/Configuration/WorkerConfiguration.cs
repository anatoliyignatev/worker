﻿namespace Worker.Core.Configuration;

/// <summary>
/// Конфигурация воркера
/// </summary>
public class WorkerConfiguration
{
    /// <summary>
    /// Конфигурация БД
    /// </summary>
    public DbOptions DbOptions { get; set; }
    
    /// <summary>
    /// Конфигурация Redis
    /// </summary>
    public RedisOptions RedisOptions { get; set; }
    
    /// <summary>
    /// Конфигурация RabbitMQ
    /// </summary>
    public RabbitMqOptions RabbitMqOptions { get; set; }
}