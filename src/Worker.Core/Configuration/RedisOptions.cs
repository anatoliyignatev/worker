﻿namespace Worker.Core.Configuration;

/// <summary>
/// Конфигурация Redis
/// </summary>
public class RedisOptions
{
    /// <summary>
    /// Строка подключения
    /// </summary>
    public string Address { get; set; }
}