﻿namespace Worker.Core.Configuration;

/// <summary>
/// Конфигурация БД
/// </summary>
public class DbOptions
{
    /// <summary>
    /// Строка подключения
    /// </summary>
    public string ConnectionString { get; set; }

    /// <summary>
    /// Диалект БД
    /// </summary>
    public string Dialect { get; set; }
}