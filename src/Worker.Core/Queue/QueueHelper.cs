﻿using Worker.Tasks.Tasks.Abstract;

namespace Worker.Core.Queue;

public class QueueHelper
{
    /// <summary>
    /// Словарь для (ИД задачи, Название задачи)
    /// </summary>
    public static readonly Dictionary<int, string> DictionaryTaskNames = new();
        
    /// <summary>
    /// Словарь для (ИД задачи, Тип задачи)
    /// </summary>
    private static readonly Dictionary<int, Type> DictionaryTaskTypes = new();
        
    /// <summary>
    /// Словарь для (Тип задачи, Код задачи)
    /// </summary>
    private static readonly Dictionary<Type, int> DictionaryTaskIdTypes = new();

    /// <summary>
    /// Словарь для (Название задачи, Код задачи)
    /// </summary>
    private static readonly Dictionary<string, int> DictionaryTaskId = new();
    
    static QueueHelper()
    {
        SelectRegisteredTaskTypes();
    }
    
    
    /// <summary>
    /// Регистрируем задачи
    /// </summary>
    public static void SelectRegisteredTaskTypes()
    {
        var taskTypes = AppDomain.CurrentDomain.GetAssemblies()
            .SelectMany(x => x.GetTypes())
            .Where(x => typeof(WorkerBaseTask).IsAssignableFrom(x))
            .Where(x => !x.IsAbstract).ToList();

        foreach (var taskType in taskTypes)
        {
            var instance = (WorkerBaseTask) Activator.CreateInstance(taskType);

            DictionaryTaskTypes.Add(instance.TaskTypeId, taskType);
            DictionaryTaskIdTypes.Add(taskType, instance.TaskTypeId);
            DictionaryTaskNames.Add(instance.TaskTypeId, instance.TaskName);
            DictionaryTaskId.Add(instance.TaskName, instance.TaskTypeId);
        }
    }
}