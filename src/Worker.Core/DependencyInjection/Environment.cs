﻿using System.Reflection;
using Autofac;
using Autofac.Core.Registration;
using Bus.Transport.Context;
using Bus.Transport.Transport;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RawRabbit.Configuration;
using RawRabbit.Configuration.Exchange;
using Worker.Core.Configuration;
using Worker.Utils.Utils;
using Module = Autofac.Module;

namespace Worker.Core.DependencyInjection;

/// <summary>
/// Статический класс для работы с окружением
/// </summary>
public static class Environment
{
    /// <summary>
    /// Префикс приложения
    /// </summary>
    public static string AppPrefix => "Worker";
    
    /// <summary>
    /// Паттерн поиска dll
    /// </summary>
    private static string DllsSearchPattern => $"{AppPrefix}.*.dll";

    /// <summary>
    /// Основной контейнер
    /// </summary>
    public static IComponentContext? Context => _isInit 
        ? _context
        : throw new Exception("Не была проведена инициализация контейнера!");
    
    /// <summary>
    /// Основной контейнер
    /// </summary>
    private static IComponentContext? _context;
    
    /// <summary>
    /// Была ли инициализация ?
    /// </summary>
    private static bool _isInit = false;
    
    /// <summary>
    /// Инициализировать внутренний контейнер
    /// </summary>
    /// <param name="host">Хост приложения</param>
    public static void Init(IHost host)
    {
        if (_isInit) return;
        if (host == default) throw new ArgumentException(nameof(host));

        _context = host.Services.GetService<IComponentContext>();
        _isInit = true;
    }

    /// <summary>
    /// Зарегистрировать модуля для Autofac
    /// </summary>
    /// <param name="context">Контекст</param>
    /// <param name="builder">Строитель контейнера</param>
    public static void RegisterModules(HostBuilderContext context, ContainerBuilder builder)
    {
        LoadAssemblies(DllsSearchPattern);
        
        foreach (var type in AssemblyUtils.GetAllExportTypesBasedOn<Module>())
        {
            var module  = (Module) Activator.CreateInstance(type);

            try
            {
                if (module != null) builder.RegisterModule(module);
            }
            catch (ComponentNotRegisteredException ex)
            {
                Console.WriteLine(
                    $"Компонет реализующий базовый тип {nameof(Module)} с типом {module} был зарегистрирован ранее." +
                    $"\nОшибка: {ex.Message}");
                    
                throw;
            }
        }
    }

    /// <summary>
    /// Загрузить сборки по паттерну
    /// </summary>
    /// <param name="pattern">Паттерн поиска названий</param>
    public static void LoadAssemblies(string pattern)
    {
        var dlls = new DirectoryInfo($"{AppDomain.CurrentDomain.BaseDirectory}")
            .GetFiles(pattern)
            .Select(x => AssemblyName.GetAssemblyName(x.FullName))
            .ToList();
        
        dlls.ForEach(dll => Assembly.Load(dll));
    }
    
    /// <summary>
    /// Получить локальную настройку для очереди
    /// </summary>
    /// <param name="configuration">Конфигурация воркера</param>
    /// <param name="timeout">Таймаут (по-дефолту 10 секунд)</param>
    /// <returns>Конфигурация очереди</returns>
    public static RawRabbitConfiguration GetLocalRawRabbitConfiguration(WorkerConfiguration configuration, int timeout = 10)
    {
        return new RawRabbitConfiguration
        {
            Hostnames = new List<string> {configuration.RabbitMqOptions.Address},
            Port = 5672,
            Username = configuration.RabbitMqOptions.UserName,
            Password = configuration.RabbitMqOptions.Password,
            RequestTimeout = TimeSpan.FromSeconds(timeout),
            PublishConfirmTimeout = TimeSpan.FromMilliseconds(1000),
            Queue = new GeneralQueueConfiguration
            {
                AutoDelete = true,
                Durable = false
            },
            Exchange = new GeneralExchangeConfiguration
            {
                Type = ExchangeType.Topic,
                AutoDelete = false,
                Durable = false
            }
        };
    }
    
    /// <summary>
    /// Создать транспортную точку
    /// </summary>
    /// <param name="config">Конфиг</param>
    /// <returns>Транспортная точка</returns>
    public static TransportPoint<BaseMessageContext> CreatePoint(RawRabbitConfiguration config)
    {
        return TransportPointFactory.CreatePoint(config, null);
    }
    
    /// <summary>
    /// Получить значение переменной среды
    /// </summary>
    /// <typeparam name="T">Тип значения</typeparam>
    /// <param name="key">Ключ</param>
    /// <param name="defaultValue">Значение при отсутствии параметра</param>
    /// <returns>Значение переменой среды</returns>
    public static T GetEnvVariableValue<T>(string key, T defaultValue = default)
    {
        var value = System.Environment.GetEnvironmentVariable(key);
        
        if (string.IsNullOrEmpty(value)) return defaultValue;
        
        var nullableType = Nullable.GetUnderlyingType(typeof(T));
        return (T) Convert.ChangeType(value, nullableType ?? typeof(T));
    }
}