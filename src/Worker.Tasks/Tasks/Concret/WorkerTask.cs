﻿using Worker.Tasks.Tasks.Abstract;

namespace Worker.Tasks.Tasks.Concret;

public class WorkerTask : WorkerBaseTask
{
    public override int TaskTypeId => 1;

    public override string TaskName => "Базовая задача воркера";
}