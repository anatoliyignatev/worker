﻿namespace Worker.Tasks.Tasks.Abstract;

/// <summary>
/// Базовая сущность для очереди сообщений
/// </summary>
public class BaseQueuedEntity
{
    /// <summary>
    /// Уникальный идентификатор задачи
    /// </summary>
    public Guid Id { get; set; }
    
    /// <summary>
    /// Время добавления задачи в очередь
    /// </summary>
    public DateTime CreatedTime { get; internal set; }

    /// <summary>
    /// Время запуска задачи (когда задача начала исполняться)
    /// </summary>
    public DateTime? StartedTime { get; internal set; }
}