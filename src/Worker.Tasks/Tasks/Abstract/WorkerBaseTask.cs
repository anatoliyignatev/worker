﻿namespace Worker.Tasks.Tasks.Abstract;

public abstract class WorkerBaseTask : BaseQueuedEntity
{
    /// <summary>
    /// Тип задачи
    /// </summary>
    public abstract int TaskTypeId { get; }
        
    /// <summary>
    /// Наименование типа задачи
    /// </summary>
    public abstract string TaskName { get; }
}